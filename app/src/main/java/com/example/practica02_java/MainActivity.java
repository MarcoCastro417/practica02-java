package com.example.practica02_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    EditText txtAltura, txtPeso;
    TextView txtImc, lblRecomendacion;
    Button idCalcular, idLimpiar, idCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        txtImc = findViewById(R.id.txtImc);
        lblRecomendacion = findViewById(R.id.lblRecomendacion);
        idCalcular = findViewById(R.id.idCalcular);
        idLimpiar = findViewById(R.id.idLimpiar);
        idCerrar = findViewById(R.id.idCerrar);

        idCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Validar
                if ((txtAltura.getText().toString().matches("")) || (txtPeso.getText().toString().matches(""))){
                    Toast.makeText(MainActivity.this, "Faltó capturar información", Toast.LENGTH_SHORT).show();
                }else{
                    calcularImc();
                }
            }
        });

        idLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiarCampos();
            }
        });

        idCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void calcularImc() {
        double altura = Double.parseDouble(txtAltura.getText().toString()) / 100;
        double peso = Double.parseDouble(txtPeso.getText().toString());
        double imc = peso / (altura * altura);

        DecimalFormat formato = new DecimalFormat("#.#");
        String resultado = formato.format(imc);

        txtImc.setText(resultado);

        Double resultadoRec = Double.parseDouble(resultado);
        if (resultadoRec < 18.5) {
            lblRecomendacion.setText("Bajo Peso");
        } else if (resultadoRec < 24.9) {
            lblRecomendacion.setText("Peso saludable");
        } else if (resultadoRec < 29.9) {
            lblRecomendacion.setText("Sobrepeso");
        } else if (resultadoRec > 30.0) {
            lblRecomendacion.setText("Obesidad");
        }
    }

    private void limpiarCampos() {
        txtAltura.setText("");
        txtPeso.setText("");
        txtImc.setText("Su IMC es: ");
    }
}